/* 
 * File:   main.c
 * Author: aussie
 *
 * Created on January 19, 2015, 12:02 PM
 */

#ifndef __AVR_ATmega162__
#define __AVR_ATmega162__
#endif
#ifndef F_CPU
#define F_CPU 8000000L
#endif

#include <stdio.h>
#include <stdlib.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "hw.h"
#include "timers.h"
#include "messages.h"
#include "kbd_read.h"
#include "lamp_control.h"
#include "uart.h"

//определяем порт и бит к которому подключено устройство 1-wire
#define W1_PORT PORTC
#define W1_DDR DDRC
#define W1_PIN PINC
#define W1_BIT 0


//функция определяет есть ли устройство на шине

unsigned char w1_find() {
    unsigned char device;
    W1_DDR |= 1 << W1_BIT; //логический "0"
    _delay_us(485); //ждем минимум 480мкс
    W1_DDR &= ~(1 << W1_BIT); //отпускаем шину
    _delay_us(65); //ждем минимум 60мкс и смотрим что на шине

    if ((W1_PIN & (1 << W1_BIT)) == 0x00)
        device = 1;
    else
        device = 0;
    _delay_us(420); //ждем оставшееся время до 480мкс
    return device;
}

//функция посылает команду на устройство 1-wire

void w1_sendcmd(unsigned char cmd) {
    for (unsigned char i = 0; i < 8; i++)//в цикле посылаем побитно
    {
        if ((cmd & (1 << i)) == 1 << i)//если бит=1 посылаем 1
        {
            W1_DDR |= 1 << W1_BIT;
            _delay_us(2);
            W1_DDR &= ~(1 << W1_BIT);
            _delay_us(65);
        } else//иначе посылаем 0
        {

            W1_DDR |= 1 << W1_BIT;
            _delay_us(65);
            W1_DDR &= ~(1 << W1_BIT);
            _delay_us(5);
        }
    }
}

//функция читает один байт с устройства 1-wire

unsigned char w1_receive_byte() {
    unsigned char data = 0;
    for (unsigned char i = 0; i < 8; i++) {//в цикле смотрим что на шине и сохраняем значение
        W1_DDR |= 1 << W1_BIT;
        _delay_us(2);
        W1_DDR &= ~(1 << W1_BIT);
        _delay_us(7);
        if ((W1_PIN & (1 << W1_BIT)) == 0x00)
            data &= ~(1 << i);
        else
            data |= 1 << i;
        _delay_us(50); //задержка до окончания тайм-слота
    }
    return data;
}

//функция преобразует полученные с датчика 18b20 данные в температуру

int temp_18b20() {
    unsigned char data[2];
    int temp = 0;
    if (w1_find() == 1) {//если есть устройство на шине
        w1_sendcmd(0xcc); //пропустить ROM код, мы знаем, что у нас одно устройство или передаем всем
        w1_sendcmd(0x44); //преобразовать температуру
        _delay_ms(750); //преобразование в 12 битном режиме занимает 750ms
        w1_find(); //снова посылаем Presence и Reset
        w1_sendcmd(0xcc);
        w1_sendcmd(0xbe); //передать байты ведущему(у 18b20 в первых двух содержится температура)
        data[0] = w1_receive_byte(); //читаем два байта с температурой
        data[1] = w1_receive_byte();
        //загоняем в двух байтную переменную
        temp = data[1];
        temp = temp << 8;
        temp |= data[0];
        //переводим в градусы
        temp *= 0.0625; //0.0625 градуса на единицу данных
    }
    //возвращаем температуру
    return temp;
}

int temp = 0;

void readTemp() {
    temp = temp_18b20();
    if (temp > 1000)//если температура <0
    {
        temp = 4096 - temp;
        temp = -temp;
    }
}

FILE uart_stream = FDEV_SETUP_STREAM(uart_putc, uart_getc, _FDEV_SETUP_RW);

int main(void) {
    stdout = stdin = &uart_stream;
    SW_DDR = 0xFF;
    LED_DDR = 0xFF;

    initUSART(9600);

    initTimers();
    initGTimers();
    initMessages();
    initHWTimer();

    initKBDRead();
    initLampControl();

    puts("test\r\n");

#define BUF_SIZE 64
    char buf[BUF_SIZE];
    unsigned char buf_cnt;

    while (1) {
        char c;
        processKBDRead();
        processLampControl();

        processMessages();

        if ((c = getchar()) != EOF) {
            if (c == '\r' || c == '\n') {
                buf[buf_cnt] = '\0';
                //nmea_parser(buf);
                buf_cnt = 0;
            } else if (buf_cnt < BUF_SIZE) {
                buf[buf_cnt] = c;
                buf_cnt++;
            }
        }

        //        	int temp = temp_18b20();
        //        	if (temp > 1000)//если температура <0
        //        	{
        //        	    temp = 4096 - temp;
        //        	    temp = -temp;
        //        	}
    }

    return 0;
}

