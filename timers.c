
#include "timers.h"
#include "hw.h"
#include <avr/io.h>
#include <avr/interrupt.h>

uint32_t gTimers[MAX_GTIMERS]; //В массиве хранятся текущие значения глобальных таймеров
uint32_t timers[MAX_TIMERS]; //В массиве хранятся текущие значения локальных таймеров

//Состояния таймера
#define TIMER_STOPPED 0 //Таймер остановлен
#define TIMER_RUNNING 1 //Таймер работает
#define TIMER_PAUSED 2 //Таймер на паузе
uint8_t GTStates[MAX_GTIMERS]; //В массиве хранятся текущие состояния глобальных таймеров
uint8_t tStates[MAX_TIMERS]; //В массиве хранятся текущие состояния локальных таймеров

// Инициализация локальных таймеров

void initTimers(void) {
    for (uint8_t i = 0; i < MAX_TIMERS; i++) {
	tStates[i] = TIMER_STOPPED;
    }
}

// Инициализация глобальных таймеров

void initGTimers(void) {
    for (uint8_t i = 0; i < MAX_GTIMERS; i++)
	GTStates[i] = TIMER_STOPPED;
}

void startTimer(uint8_t timerID) {
    if (tStates[timerID] == TIMER_STOPPED) {
	timers[timerID] = 0;
	tStates[timerID] = TIMER_RUNNING;
    }
}

void restartTimer(uint8_t timerID) {
    if (tStates[timerID] == TIMER_RUNNING) {
	timers[timerID] = 0;
    }
}

// Запуск таймера

void startGTimer(uint8_t GTimerID) {
    if (GTStates[GTimerID] == TIMER_STOPPED) {
	gTimers[GTimerID] = 0;
	GTStates[GTimerID] = TIMER_RUNNING;
    }
}

uint32_t getTimer(uint8_t timerID) {
    return timers[timerID];
}

//Получение текущего значения таймера

uint32_t getGTimer(uint8_t GTimerID) {
    return gTimers[GTimerID];
}

void resetTimer(uint8_t timerID) {
    tStates[timerID] = TIMER_STOPPED;
    timers[timerID] = 0;
}

//Останов таймера

void stopGTimer(uint8_t GTimerID) {
    GTStates[GTimerID] = TIMER_STOPPED;
}

//Приостановка работы таймера

void pauseGTimer(uint8_t GTimerID) {
    if (GTStates[GTimerID] == TIMER_RUNNING)
	GTStates[GTimerID] = TIMER_PAUSED;
}

//Продолжение работы таймера

void releaseGTimer(uint8_t GTimerID) {
    if (GTStates[GTimerID] == TIMER_PAUSED)
	GTStates[GTimerID] = TIMER_RUNNING;
}

void processTimers(void) {
    uint8_t i;

    for (i = 0; i < MAX_TIMERS; i++) {
	if (tStates[i] == TIMER_RUNNING) {
	    timers[i]++;
	}
    }

    for (i = 0; i < MAX_GTIMERS; i++) {
	if (GTStates[i] == TIMER_RUNNING) {
	    gTimers[i]++;
	}
    }
}

void initHWTimer(void) {
    cli();
    //        	DDRB = 0x02; //настраиваем OC1A как выход
    //    	PORTB = 0x00;
    TCCR1A = 0x0; //при совпадении уровень OC1A меняется на противоположный
    TCCR1B = 0x02; //CLK/8
    
    OCR1AH = 0x03; //записываем в регистр OCR1A 1000 = 1ms
    OCR1AL = 0xE8;
//    OCR1AH = 0;
//    OCR1AL = 1; // 1 в регистр OCR1A = таймер в 1мкс
    
    TIMSK |= _BV(OCIE1A); //разрешаем прерывание по совпадению
    sei();
    //разрешаем прерывания глобально
}

// обработчик прерывания по совпадению А
// обработчик прерывания таймера/счетчика
ISR(TIMER1_COMPA_vect) {
    TCNT1H = 0; //обнуляем регистр TCNT1
    TCNT1L = 0;

    processTimers();
}