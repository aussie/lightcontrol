/* 
 * File:   lamp_control.h
 * Author: aussie
 *
 * Created on January 24, 2015, 12:33 PM
 */

#ifndef LAMP_CONTROL_H
#define	LAMP_CONTROL_H

#include "hw.h"
#include "messages.h"
#include "timers.h"
#include "kbd_read.h"
#include "uart.h"

void initLampControl();
void processLampControl();


#endif	/* LAMP_CONTROL_H */

