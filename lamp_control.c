#include "lamp_control.h"

uint8_t lc_state;
uint8_t cnt;
uint8_t _key;

#define SW_DELAY ms*300
#define LAMP_CNT 3

void initLampControl() {
    lc_state = 0;
    cnt = 0;
    _key = 0;
}

void processLampControl() {
    uint8_t key = getKeyCode();

    switch (lc_state) {
        case 0:
            // состояние 0 - читаем нажатие кнопки, старутем таймер таймаута кнопки
            if (getMessage(MSG_KEY_PRESSED)) {
                startTimer(SW_TIMER);
                lc_state = 1;
                cnt++;
                _key = key;
            }
            if (getMessage(MSG_LONG_KEY_PRESSED)) {
                lc_state = 3;
            }
            break;
        case 1:

            // состояние 1 - если таймаут не вышел, и еще раз была нажата кнопка, увеличиваем счетчик нажатий
            if (getMessage(MSG_LONG_KEY_PRESSED)) {
                lc_state = 3;
            } else {
                if (getTimer(SW_TIMER) < SW_DELAY) {
                    if (getMessage(MSG_KEY_PRESSED)) {
                        cnt = cnt == LAMP_CNT ? cnt = 0 : cnt + 1;
                        restartTimer(SW_TIMER);
                    }
                } else {
                    lc_state = 2;
                }
            }

            break;
        case 2:
            // состояние 2 - включение\выключение соответсвующих ламп
            if (key & _BV(SW1_PIN)) {
                puts("SWITCH 1\n\r");
                switch (cnt) {
                    case 1:
                        LAMP_PORT ^= 1 << LAMP1_PIN;
                        LED_PORT ^= 1 << R_PIN;
                        break;
                    case 2:
                        LAMP_PORT ^= 1 << LAMP2_PIN;
                        LED_PORT ^= 1 << G_PIN;
                        break;
                    case 3:
                        LAMP_PORT ^= 1 << LAMP3_PIN;
                        LED_PORT ^= 1 << B_PIN;
                        break;
                }
            }
            if (key & _BV(SW2_PIN)) {
                puts("SWITCH 2\n\r");
                switch (cnt) {
                    case 1:
                        LAMP_PORT ^= 1 << LAMP2_PIN;
                        LED_PORT ^= 1 << G_PIN;
                        break;
                    case 2:
                        LAMP_PORT ^= (1 << LAMP1_PIN);
                        LED_PORT ^= 1 << R_PIN;
                        break;
                    case 3:
                        LAMP_PORT ^= (1 << LAMP3_PIN);
                        LED_PORT ^= 1 << B_PIN;
                        break;
                }
            }
            if (key & _BV(SW3_PIN)) {
                puts("SWITCH 3\n\r");
                switch (cnt) {
                    case 1:
                        LAMP_PORT ^= (1 << LAMP3_PIN);
                        LED_PORT ^= 1 << B_PIN;
                        break;
                    case 2:
                        LAMP_PORT ^= (1 << LAMP1_PIN);
                        LED_PORT ^= 1 << R_PIN;
                        break;
                    case 3:
                        LAMP_PORT ^= (1 << LAMP2_PIN);
                        LED_PORT ^= 1 << G_PIN;
                        break;
                }
            }

            cnt = 0;
            resetTimer(SW_TIMER);
            lc_state = 0;

            break;
        case 3:
            // состояние 3 - обработка длинного нажатия. выключаем все, видем в на состояние 0
            setPin(&LAMP_PORT, LAMP1_PIN, 0);
            setPin(&LAMP_PORT, LAMP2_PIN, 0);
            setPin(&LAMP_PORT, LAMP3_PIN, 0);
            LED_PORT &= ~_BV(R_PIN) & ~_BV(G_PIN) & ~_BV(B_PIN);
            lc_state = 0;
            cnt = 0;
            break;
    }
}