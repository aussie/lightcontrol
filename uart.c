#include "uart.h"

#define bauddivider(baudrate) (F_CPU/(16*(baudrate)-1))


FIFO(64) uart_tx_fifo;
FIFO(64) uart_rx_fifo;

void initUSART(long baudrate) {
    cli();
    //InitUSART
    UBRR0L = LO(bauddivider(baudrate));
    UBRR0H = HI(bauddivider(baudrate));
    UCSR0A = 0;
    UCSR0B = 1 << RXEN0 | 1 << TXEN0 | 1 << RXCIE0 | 1 << TXCIE0;
    UCSR0C = 1 << URSEL0 | 1 << UCSZ00 | 1 << UCSZ01;
}

/**
 * Обработчик прерывания по окончанию приёма байта
 */
ISR(USART0_RXC_vect) {
    unsigned char rxbyte = UDR0;
    if (!FIFO_IS_FULL(uart_rx_fifo)) {
        FIFO_PUSH(uart_rx_fifo, rxbyte);
    }
}

ISR(USART0_TXC_vect) {
    
}

ISR(USART0_UDRE_vect) {
    if (FIFO_IS_EMPTY(uart_tx_fifo)) {
        //если данных в fifo больше нет то запрещаем это прерывание
        UCSR0B &= ~(1 << UDRIE0);
    } else {
        //иначе передаем следующий байт
        char txbyte = FIFO_FRONT(uart_tx_fifo);
        FIFO_POP(uart_tx_fifo);
        UDR0 = txbyte;
    }
}

int uart_putc(char c, FILE *file) {
    int ret;
    cli(); //запрещаем прерывания
    if (!FIFO_IS_FULL(uart_tx_fifo)) {
        //если в буфере есть место, то добавляем туда байт
        FIFO_PUSH(uart_tx_fifo, c);
        //и разрешаем прерывание по освобождению передатчика
        UCSR0B |= (1 << UDRIE0);
        ret = 0;
    } else {
        ret = -1; //буфер переполнен
    }
    sei(); //разрешаем прерывания
    return ret;
}

int uart_getc(FILE* file) {
    int ret;
    cli(); //запрещаем прерывания
    if (!FIFO_IS_EMPTY(uart_rx_fifo)) {
        //если в буфере есть данные, то извлекаем их
        ret = FIFO_FRONT(uart_rx_fifo);
        FIFO_POP(uart_rx_fifo);
    } else {
        ret = _FDEV_EOF; //данных нет
    }
    sei(); //разрешаем прерывания
    return ret;
}


//FILE uart_stream = FDEV_SETUP_STREAM(uart_putc, uart_getc, _FDEV_SETUP_RW);

//int main( void )
//{
//  stdout = stdin = &uart_stream;
//  uart_init();
//  sei();
//  puts( "Hello world\r\n" );
//  while( 1 ) {
//    int c = getchar();
//    if( c != EOF ) {
//      putchar( c );
//    }
//  }
//  return 0;
//}

