#ifndef _OW_BYTEFUNC_H_
#define _OW_BYTEFUNC_H_

/*
Software implementation of 1-Wire interface,
byte-level module.

Written by YS from we.easyelectronics.ru

You may freely use and redistribute this code any way you want,
but only at your own risk. Author gives no warranty and takes no 
responsibility on anything that could happen while using this code.
*/

#include <avr/io.h>
#include <stdint.h>
#include <util/crc16.h>

#include "owbitfunc.h"

//Standard 1-Wire commands
#define OW_READ_ROM		0x33
#define OW_MATCH_ROM	0x55
#define OW_SKIP_ROM		0xCC

//1-Wire bus initialization
void OW_InitBus(void);

//Writes byte to 1-Wire bus
void OW_WriteByte(uint8_t byte);

//Reads byte from 1-Wire bus
uint8_t OW_ReadByte(void);

//Reads current device's address
//and checks CRC; returns 1 if correct,
//0 otherwise.
uint8_t OW_ReadRom(uint8_t rom_addr[8]);

#endif
