#include "hw.h"

void setPin(volatile uint8_t *port, uint8_t pin, uint8_t state) {
    if (state) {
	*port |= 1 << pin; // enable pin
    } else {
	*port &= ~(1 << pin); // disable pin
    }
}

void ledStatus(uint8_t ledPin, uint8_t state) {
    setPin(&LED_PORT, ledPin, state);
}