#ifndef _OW_BITFUNC_H_
#define _OW_BITFUNC_H_

/*
Software implementation of 1-Wire interface,
bit-level module.

Written by YS from we.easyelectronics.ru

You may freely use and redistribute this code any way you want,
but only at your own risk. Author gives no warranty and takes no 
responsibility on anything that could happen while using this code.
*/

//CPU frequency in MHz
//F_CPU/1000000[UL] won't work - assembler does not
//recognize 'UL' suffix in F_CPU.
#define F_CPU_MHz	8

//1-Wire hardware settings

//1-Wire port registers
#define OW_PORT		PORTB
#define OW_DDR		DDRB
#define OW_PIN		PINB

//1-Wire bus pin bitmask
#define OW_BUSMSK	(1<<PB0)	

#ifndef __ASSEMBLER__

//Checks for PRESENCE, returns 0xFF if device detected,
//0 otherwise.
uint8_t OW_Presence(void);

//Send bit to 1-Wire bus
void _ow_send_1(void);
void _ow_send_0(void);

//Reads bit from 1-Wire bus.
//Returns 0x00 if 0 received, 0xFF if 1.
uint8_t _ow_receive_bit(void); 

#endif

#endif
