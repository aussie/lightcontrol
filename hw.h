/* 
 * File:   hw.h
 * Author: aussie
 *
 * Created on January 19, 2015, 11:34 PM
 */

#ifndef HW_H
#define	HW_H

#include <stdint.h>
#include <avr/io.h>

#define HI(x) ((x)>>8)
#define LO(x) ((x)& 0xFF)
#define ON 1
#define OFF 0


// define hardware definitions
#define LED_PORT PORTB
#define LED_DDR DDRB
#define R_PIN 4
#define G_PIN 3
#define B_PIN 2

#define SW_PORT PORTC
#define SW_DDR DDRC
#define SW_PIN PINC

#define SW1_PIN 2
#define SW2_PIN 3
#define SW3_PIN 4

#define LAMP_PORT PORTC
#define LAMP1_PIN 5
#define LAMP2_PIN 6
#define LAMP3_PIN 7

#define LAMP_DDR DDRC


void setPin(volatile uint8_t *port, uint8_t pin, uint8_t state);
void ledStatus(uint8_t ledPin, uint8_t state);
#endif	/* HW_H */

