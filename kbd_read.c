#include "kbd_read.h"
#include "messages.h"

uint8_t kbd_state = 0;
uint8_t data, _data;

//период таймера 1 мс
#define DEBOUNCE ms*5 //задержка для подавления дребезга
#define SHORT_KEY ms*100 //задержка для короткого нажатия клавиши
#define LONG_KEY ms*500 //задержка для длинного нажатия клавиши

void initKBDRead() {
    kbd_state = 0;
    // set input mode for switchers pins
    SW_DDR &= ~(1 << SW1_PIN) & ~(1 << SW2_PIN) & ~(1 << SW3_PIN);
}

void processKBDRead() {
    uint8_t data = SW_PIN;

    switch (kbd_state) {
        case 0:
            // состояние 0. В этом состоянии автомат ожидает нажатия на клавишу
            if (bit_is_set(data, SW1_PIN) || bit_is_set(data, SW2_PIN) || bit_is_set(data, SW3_PIN)) {
                startTimer(KEYB_TIMER);
                kbd_state = 1;
                _data = data;
            }
            break;
        case 1:
            // состояние 1. В этом состоянии вводится задержка на время debounce (ожидание завершения переходного процесса 
            // при замыкании контактов клавиши)
            if (getTimer(KEYB_TIMER) >= DEBOUNCE) {
                kbd_state = 2;
            }
            break;
        case 2:
            // состояние 2. В этом состоянии фиксируется факт нажатия клавиши и начинается отсчет времени удержания клавиши
            if (data == _data) {
                restartTimer(KEYB_TIMER);
                kbd_state = 3;
            } else {
                kbd_state = 0;
            }
            break;
        case 3:
            // состояние 3. отпустили клавишу. В зависимости от времени удержания выдаем событие длинного или короткого нажатия
            if (data != _data) {
                if (getTimer(KEYB_TIMER) >= LONG_KEY) {
                    sendMessage(MSG_LONG_KEY_PRESSED);
                } else {
                    sendMessage(MSG_KEY_PRESSED);
                }
                kbd_state = 0;
                resetTimer(KEYB_TIMER);
            }
            break;
    }
}

uint8_t getKeyCode() {
    return _data;
}