#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU_AVR-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU_AVR-Linux-x86
CND_ARTIFACT_NAME_Debug=lightcontrol
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU_AVR-Linux-x86/lightcontrol
CND_PACKAGE_DIR_Debug=dist/Debug/GNU_AVR-Linux-x86/package
CND_PACKAGE_NAME_Debug=lightcontrol.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU_AVR-Linux-x86/package/lightcontrol.tar
# Release configuration
CND_PLATFORM_Release=AVR_GNU-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release/AVR_GNU-Linux-x86
CND_ARTIFACT_NAME_Release=lightcontrol
CND_ARTIFACT_PATH_Release=dist/Release/AVR_GNU-Linux-x86/lightcontrol
CND_PACKAGE_DIR_Release=dist/Release/AVR_GNU-Linux-x86/package
CND_PACKAGE_NAME_Release=lightcontrol.tar
CND_PACKAGE_PATH_Release=dist/Release/AVR_GNU-Linux-x86/package/lightcontrol.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
