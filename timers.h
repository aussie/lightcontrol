/* 
 * File:   timers.h
 * Author: aussie
 *
 * Created on January 19, 2015, 12:04 PM
 */

#ifndef TIMERS_H_
#define TIMERS_H_

#include <stdint.h>

#define MAX_GTIMERS 2 // Максимальное количество глобальных таймеров в системе
#define MAX_TIMERS 4 // Максимальное количество локальных таймеров в системе

// Идентификаторы глобальных таймеров

#define SW_TIMER 0
#define KEYB_TIMER 1
#define GTIMER1 2
#define GTIMER2 3

#define us 1
#define ms 1 // период аппаратного таймера 1 мкс
#define sec 1000 * ms 
#define min 60 * sec
#define hour 60 * min
#define day 24 * hour

// инициализация аппаратного таймера
void initHWTimer(void);

// функци  для работы с локальным таймером
void initTimers(void);
void resetTimer(uint8_t timerID);
void startTimer(uint8_t timerID);
uint32_t getTimer(uint8_t timerID);
void restartTimer(uint8_t timerID);

// функции работы с глобальными таймерами
void initGTimers(void); //Ин(ициализация глобальных таймеров
void startGTimer(uint8_t GTimerID); //Запуск таймера
void stopGTimer(uint8_t GTimerID); //Останов таймера
void pauseGTimer(uint8_t GTimerID); //Приостановка работы таймера
void releaseGTimer(uint8_t GTimerID); //Продолжение работы таймера
uint32_t getGTimer(uint8_t GTimerID); //Получение текущего значения таймера

#endif /* TIMERS_H_ */