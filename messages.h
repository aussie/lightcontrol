/* 
 * File:   messages.h
 * Author: aussie
 *
 * Created on January 19, 2015, 12:04 PM
 */

#ifndef MESSAGES_H_
#define MESSAGES_H_

#include <stdint.h>

#define MAX_MESSAGES 64
#define MAX_BROADCAST_MESSAGES 64

#define MSG_KEY_PRESSED 0
#define MSG_LONG_KEY_PRESSED 1

void initMessages(void);
void sendMessage(uint8_t msg);
void sendBroadcastMessage(uint8_t msg);
uint8_t getMessage(uint8_t msg);
uint8_t getBroadcastMessage(uint8_t msg);
void processMessages(void);

typedef struct {
    char msg;
    void *paramPtr;
} MSG_DATA;

#endif /* MESSAGES_H_ */