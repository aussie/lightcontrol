#include "messages.h"

//MSG_DATA messages[MAX_MESSAGES];
uint8_t messages[MAX_MESSAGES];
uint8_t broadcastMessages[MAX_BROADCAST_MESSAGES];

void initMessages(void) {
    uint8_t i;

    for (i = 0; i < MAX_MESSAGES; i++) {
        messages[i] = 0;
    }

    for (i = 0; i < MAX_BROADCAST_MESSAGES; i++) {
        broadcastMessages[i] = 0;
    }
}

void sendMessage(uint8_t msg) {
    messages[msg] = 1;
}

void sendBroadcastMessage(uint8_t msg) {
    broadcastMessages[msg] = 1;
}

void processMessages(void) {
    uint8_t i;

    for (i = 0; i < MAX_MESSAGES; i++) {
        if (messages[i] == 2)
            messages[i] = 0;
        if (messages[i] == 1)
            messages[i] = 2;
    }

    for (i = 0; i < MAX_BROADCAST_MESSAGES; i++) {
        if (broadcastMessages[i] == 2)
            broadcastMessages[i] = 0;
        if (broadcastMessages[i] == 1)
            broadcastMessages[i] = 2;
    }
}

uint8_t getMessage(uint8_t msg) {
    if (messages[msg] == 2) {
        messages[msg] = 0;
        return 1;
    };
    return 0;
}

uint8_t getBroadcastMessage(uint8_t msg) {
    if (broadcastMessages[msg] == 2) {
        return 1;
    }

    return 0;
}

