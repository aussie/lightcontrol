/* 
 * File:   kbd_read.h
 * Author: aussie
 *
 * Created on January 19, 2015, 11:32 PM
 */

#ifndef KBD_READ_H
#define	KBD_READ_H

#include "hw.h"
#include "messages.h"
#include "timers.h"

void initKBDRead();
void processKBDRead();
uint8_t getKeyCode();

#endif	/* KBD_READ_H */

