/*
Software implementation of 1-Wire interface,
byte-level module source.

See owbytefunc.h for complete info.
*/

#include "owbytefunc.h"

void OW_InitBus(void)
{
  OW_DDR&=~OW_BUSMSK;
  OW_PORT&=~OW_BUSMSK;
}

void OW_WriteByte(uint8_t byte)
{
  uint8_t i;

  for (i=1; i>0; i=i<<1)
  {
    if (byte & i)
	  _ow_send_1();
    else
	  _ow_send_0();
  }
}

uint8_t OW_ReadByte(void)
{
  uint8_t byte=0,i;

  for (i=0; i<8; i++)
  {
    if (_ow_receive_bit())
	  byte|=(1<<i);
  }

  return byte;
}

uint8_t OW_ReadRom(uint8_t rom_addr[8])
{
  uint8_t i,CRC=0;

  OW_WriteByte(OW_READ_ROM);

  for (i=0; i<8; i++)
    rom_addr[7-i]=OW_ReadByte();

  for (i=0; i<8; i++)
    CRC=_crc_ibutton_update(CRC,rom_addr[7-i]);

  return !(CRC);
}
