/* 
 * File:   uart.h
 * Author: aussie
 *
 * Created on January 26, 2015, 10:48 PM
 */

#ifndef UART_H
#define	UART_H

#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "fifo.h"
#include "hw.h"

void initUSART(long baudrate);
int uart_getc( FILE* file );
int uart_putc(char c, FILE *file);

#endif	/* UART_H */

